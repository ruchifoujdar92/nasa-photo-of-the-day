package com.nasaphotooftheday

import android.app.DatePickerDialog
import android.os.Bundle
import android.view.View
import android.webkit.WebView
import android.widget.Button
import android.widget.ImageView
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.LiveData
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import com.nasaphotooftheday.databinding.ActivityMainBinding
import com.nasaphotooftheday.utils.ConnectivityCheck
import com.nasaphotooftheday.viewmodel.MyViewModel
import com.nasaphotooftheday.viewmodel.PhotoData
import kotlinx.android.synthetic.main.activity_main.*
import java.text.SimpleDateFormat
import java.util.*

class MainActivity : AppCompatActivity(){
    private lateinit var myViewModel: MyViewModel
    private var cal = Calendar.getInstance()
    private var button_date: Button? = null
    private var button_zoom_in: Button? = null
    private var button_zoom_out: Button? = null
    private var imageView: ImageView? = null
    private var webview: WebView? = null
    private lateinit var connect:ConnectivityCheck

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val binding: ActivityMainBinding =
            DataBindingUtil.setContentView(this, R.layout.activity_main)
        myViewModel = ViewModelProvider(this).get(MyViewModel::class.java)
        connect= ConnectivityCheck()

        // get the references from layout file
        button_date = this.calendarView_btn
        imageView = this.photo_imageview
        button_zoom_in = this.zoom_in_btn
        button_zoom_out = this.zoom_out_btn
        webview=this.videoView

       if(connect.isConnectingToInternet(this))
       {
           myViewModel.getPhotoDetails()
       }
        else{
           Toast.makeText(this@MainActivity, "Please connect to Internet", Toast.LENGTH_SHORT).show()
           progressBar.visibility = View.INVISIBLE
       }

        //view model with live data example using data binding
        val photoDataLiveData: LiveData<PhotoData> = myViewModel!!.photo
        photoDataLiveData.observe(
            this,
            Observer { photoData ->
                binding.photo = photoData
                myViewModel.bindImage(photo_imageview,photoData.hdurl,progressBar)
                if (!(photoData.url.equals("") || photoData.url==null)){
                    videoView.visibility=View.VISIBLE
                    videoView.getSettings().setLoadsImagesAutomatically(true);
                    videoView.getSettings().setJavaScriptEnabled(true);
                    videoView.loadUrl(photoData.url);
                    zoom_out_btn.visibility = View.INVISIBLE
                    zoom_in_btn.visibility = View.INVISIBLE
                }
                else{
                    videoView.visibility=View.INVISIBLE
                    zoom_in_btn.visibility = View.VISIBLE
                }
            })

        //zoom_in and zoom_out for image
        zoom_in_btn.setOnClickListener {
          val x: Float = photo_imageview.getScaleX()
          val y: Float = photo_imageview.getScaleY()
           val temp = "3.0"
            val x_temp = x.toString()

            if (x_temp.equals(temp)){
                zoom_out_btn.visibility = View.VISIBLE
                zoom_in_btn.visibility = View.INVISIBLE
            }
            else{
                photo_imageview.setScaleX(x + 1)
                photo_imageview.setScaleY(y + 1)
            }
      }
        zoom_out_btn.setOnClickListener {
            val x: Float = photo_imageview.getScaleX()
            val y: Float = photo_imageview.getScaleY()
            val temp = "1.0"
            val x_temp = x.toString()

            if (x_temp.equals(temp)){
                zoom_in_btn.visibility = View.VISIBLE
                zoom_out_btn.visibility = View.INVISIBLE
            }
            else{
                photo_imageview.setScaleX(x - 1)
                photo_imageview.setScaleY(y - 1)
            }
        }
    // create an OnDateSetListener
        val dateSetListener =
            DatePickerDialog.OnDateSetListener { view, year, monthOfYear, dayOfMonth ->
                cal.set(Calendar.YEAR, year)
                cal.set(Calendar.MONTH, monthOfYear)
                cal.set(Calendar.DAY_OF_MONTH, dayOfMonth)
                progressBar.visibility =View.VISIBLE
                selectDate()
            }
        // when you click on the button, show DatePickerDialog that is set with OnDateSetListener
        button_date!!.setOnClickListener {
            DatePickerDialog(this@MainActivity,
                dateSetListener,
                // set DatePickerDialog to point to today's date when it loads up
                cal.get(Calendar.YEAR),
                cal.get(Calendar.MONTH),
                cal.get(Calendar.DAY_OF_MONTH)).show()
        }
    }
    //when click on date from calendar
    private fun selectDate() {
        val myFormat = "yyyy-MM-dd"// mention the format you need
        val sdf = SimpleDateFormat(myFormat, Locale.US)
        val date = sdf.format(cal.getTime()).toString()
        if(connect.isConnectingToInternet(this))
        {
            myViewModel.getPhotoByDate(date)
        }
        else{
            Toast.makeText(this@MainActivity, "Please connect to Internet", Toast.LENGTH_SHORT).show()
            progressBar.visibility = View.INVISIBLE
        }
    }
}


