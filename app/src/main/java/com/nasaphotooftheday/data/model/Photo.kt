package com.nasaphotooftheday.data.model

//parameters for getting response from APIs called
data class Photo(
    val explanation: String,
    val hdurl: String,
    val media_type: String,
    val title: String,
    val url: String,
    val msg:String
)