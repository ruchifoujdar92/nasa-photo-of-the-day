package com.nasaphotooftheday.data.api

import com.nasaphotooftheday.data.model.Photo
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

/*interface consists of GET request to communicate with server using APIs*/

interface ApiService {
    //get request for getting photo of the day
    @GET("apod?")
     fun getPhoto(@Query("api_key") app_id: String): Call<Photo>

    //get request for getting photo by selecting date
    @GET("apod?")
     fun getPhotoByDate(@Query("api_key") app_id: String,
                        @Query("date") date: String): Call<Photo>
}