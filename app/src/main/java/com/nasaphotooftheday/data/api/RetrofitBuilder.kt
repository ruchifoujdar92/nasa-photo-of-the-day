package com.nasaphotooftheday.data.api

import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

//retrofit builder to construct url and make server call
object RetrofitBuilder {

    private const val BASE_URL = "https://api.nasa.gov/planetary/"

    private fun getRetrofit(): Retrofit {
        return Retrofit.Builder()
            .baseUrl(BASE_URL)
            .addConverterFactory(GsonConverterFactory.create())
            .build()
    }
//create service with base_url
    val apiService: ApiService = getRetrofit().create(ApiService::class.java)
}