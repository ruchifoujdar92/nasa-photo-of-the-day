package com.nasaphotooftheday.viewmodel

import androidx.databinding.BaseObservable
import androidx.databinding.Bindable
import com.nasaphotooftheday.BR

//two-way binding example
class PhotoData(internal var title: String?, internal var description: String?,
                internal var hdurl:String?,
                internal var url:String?) : BaseObservable() {

    @Bindable
    fun getTitle(): String? {
        return title
    }
    @Bindable
     fun getDescription(): String? {
        return description
    }

    fun setTitle(title: String?) {
        this.title = title
        notifyPropertyChanged(BR.title)
    }

    fun setDescription(description: String?) {
        this.description = description
        notifyPropertyChanged(BR.description)
    }
}