package com.nasaphotooftheday.viewmodel

import android.app.Application
import android.content.ContentValues
import android.graphics.drawable.Drawable
import android.util.Log
import android.view.View
import android.widget.ImageView
import android.widget.ProgressBar
import androidx.lifecycle.AndroidViewModel
import androidx.lifecycle.MutableLiveData
import com.bumptech.glide.Glide
import com.bumptech.glide.load.DataSource
import com.bumptech.glide.load.engine.GlideException
import com.bumptech.glide.request.RequestListener
import com.nasaphotooftheday.data.api.ApiKey
import com.nasaphotooftheday.data.api.RetrofitBuilder
import com.nasaphotooftheday.data.model.Photo
import org.json.JSONObject
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

//view model with live data
class MyViewModel(application: Application) :
    AndroidViewModel(application){
    var photo_data: MutableLiveData<PhotoData>? = null
    var title:String?=null
    var explanation:String?=null
    var hdurl:String?=null
    var media_type:String?=null
    var url:String?=null
    var message:String?=null

    //if MutableLiveData(value already present) is not null then return data otherwise create new object and create value
    val photo: MutableLiveData<PhotoData>
        get() {
            if (photo_data == null) {
                photo_data = MutableLiveData()
                createValue(title,explanation,hdurl,url)
            }
            return photo_data!!
        }

    //create value and set to view model method
    private fun createValue(title:String?, explanation: String?,
                            hdurl:String?,url:String?) {
        photo_data!!.value = PhotoData(
            title,
            explanation,
            hdurl,
            url
        )
    }

    override fun onCleared() {
        super.onCleared()
        Log.i(ContentValues.TAG, "ViewModel Destroyed")
    }
    internal fun getPhotoDetails() {
        val call = RetrofitBuilder.apiService.getPhoto(ApiKey.AppId)

        call.enqueue(object : Callback<Photo> {
            override fun onResponse(call: Call<Photo>, response: Response<Photo>) {
                var photoDetails = response.body()!!
                if (response.code() == 200) {
                    explanation = photoDetails.explanation
                    title = photoDetails.title
                    if (photoDetails.media_type.equals("image"))
                    {
                        hdurl = photoDetails.hdurl
                        url=""
                    }
                    else if(photoDetails.media_type.equals("video")) {
                        url = photoDetails.url
                        hdurl=""
                    }
                    createValue(title, explanation, hdurl, url)
                }
                else if (response.code()==400){
                    val jsonObj = JSONObject(response.errorBody()!!.charStream().readText())
                    var message = jsonObj.getString("msg")
                    createValue("", message,
                        "","")
                }
            }
            override fun onFailure(call: Call<Photo>, t: Throwable) {
                TODO("Not yet implemented")
            }
        })
    }
   internal fun getPhotoByDate(date:String) {
       val call = RetrofitBuilder.apiService.getPhotoByDate(ApiKey.AppId,date)
        call.enqueue(object : Callback<Photo> {
            override fun onResponse(call: Call<Photo>, response: Response<Photo>) {
                if (response.code() == 200) {
                    var photoDetails = response.body()!!
                    explanation = photoDetails.explanation
                    title = photoDetails.title
                    if (photoDetails.media_type.equals("image"))
                    {
                        hdurl = photoDetails.hdurl
                        url=""
                    }
                    else if(photoDetails.media_type.equals("video")){
                        url=photoDetails.url
                        hdurl=""
                    }
                    createValue(title, explanation,hdurl,url)
                }
                else if (response.code()==400){
                    val jsonObj = JSONObject(response.errorBody()!!.charStream().readText())
                    var message = jsonObj.getString("msg")
                    createValue("", message,
                        "","")
                }
            }
            override fun onFailure(call: Call<Photo>, t: Throwable) {
                t.stackTrace
                TODO("Not yet implemented")
            }
        })
    }

    fun bindImage(ivProfile: ImageView, imgUrl: String?,progressBar: ProgressBar?) {
        if (progressBar != null) {
            progressBar.visibility=View.VISIBLE
        }
        ivProfile.let {
            Glide.with(ivProfile.context)
                .load(imgUrl)
                .listener(object : RequestListener<Drawable?> {
                    override fun onLoadFailed(
                        e: GlideException?,
                        model: Any?,
                        target: com.bumptech.glide.request.target.Target<Drawable?>?,
                        isFirstResource: Boolean
                    ): Boolean {
                        if (progressBar != null) {
                            progressBar.visibility= View.GONE
                        }
                        return false
                    }
                    override fun onResourceReady(
                        resource: Drawable?,
                        model: Any?,
                        target: com.bumptech.glide.request.target.Target<Drawable?>?,
                        dataSource: DataSource?,
                        isFirstResource: Boolean
                    ): Boolean {
                        if (progressBar != null) {
                            progressBar.visibility=View.GONE
                        }
                        return false
                    }
                })
                .into(ivProfile)
        }
    }
}