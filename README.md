# NASA-Photo of the Day

NASA-Photo of the day is build on Kotlin and is also based on MVVM architecture.

This application calls NASA API to get everyday new photos and videos
1) https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY
2) https://api.nasa.gov/planetary/apod?api_key=DEMO_KEY/YYYY-MM-DD -> used to fetch data based on a particular date

It has one single screen, which contains:
1) imageView and webView to display photos and play video from API call
2) zoom-in and zoom-out button 
3) calendar button to select date and display image and video for a particular day

Libraries used:
1) Retrofit library for API calls.
2) Glide for downloading an image from url and displaying on imageview

For icons, used Flaticons which are free avaiable


